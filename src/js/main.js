const URL = 'http://localhost:3000/person';

function fetchData(url) {
  return fetch(url).then(response => response.json());
}

function addAboutMe(person) {
  let aboutMe = `<h2>about me</h2>` + `<p>${person.description}</p>`;

  document.getElementById('about-me').innerHTML = aboutMe;
}

function addEducation(person) {
  let educations = person.educations;
  let educationListItems = '';
  let education;

  educations.forEach(item => {
    educationListItems += `<li><p>${item.year}</p><div class='content'><p>${item.title}</p><p>${item.description}</p></div></li>`;
  });
  education = `<h2>education</h2>` + `<ul>${educationListItems}</ul>`;
  document.getElementById('education').innerHTML = education;
}

function parseData(data) {
  let educations = data.educations;
  let educationArray = [];
  let person;

  educations.forEach(item =>
    educationArray.push(new Education(item.year, item.title, item.description))
  );
  person = new Person(data.name, data.age, data.description, educationArray);
  return person;
}

class Education {
  constructor(year, title, description) {
    this.year = year;
    this.title = title;
    this.description = description;
  }
}

class Person {
  constructor(name, age, description, educationArray) {
    this.name = name;
    this.age = age;
    this.description = description;
    this.educations = educationArray;
  }
}

fetchData(URL)
  .then(result => {
    let person = parseData(result);

    addAboutMe(person);
    addEducation(person);
  })
  .catch(error => {
    // eslint-disable-next-line no-console
    console.error(error);
  });
